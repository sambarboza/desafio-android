package com.sambarboza.br.desafioconcretesolutions.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sambarboza.br.desafioconcretesolutions.API;
import com.sambarboza.br.desafioconcretesolutions.App;
import com.sambarboza.br.desafioconcretesolutions.R;
import com.sambarboza.br.desafioconcretesolutions.ShotsActivity;
import com.sambarboza.br.desafioconcretesolutions.model.Shot;
import com.sambarboza.br.desafioconcretesolutions.ui.FastBlurredBitmapTransformation;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Shot> mShots = new ArrayList<>();
    private Gson mGson;
    private int mCurrentPage = 1;
    private int mItemsPerPage = Integer.MAX_VALUE;
    private int mPages = Integer.MAX_VALUE;
    private boolean isLoading = false;
    protected ItemClickListener mItemClickListener;

    public ShotsAdapter(Context context, ItemClickListener itemClickListener) {
        this.mContext = context;
        this.mItemClickListener = itemClickListener;
        this.mInflater = LayoutInflater.from(context);
        this.mGson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        this.loadShots();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public @InjectView(R.id.shot_image) ImageView image;
        public @InjectView(R.id.shot_title) TextView titleTxt;
        public @InjectView(R.id.shot_views_count) TextView viewsCountTxt;
        public @InjectView(R.id.shot_comments_count) TextView commentsCountTxt;
        public @InjectView(R.id.shot_likes_count) TextView likesCountTxt;
        public @InjectView(R.id.glass_image) ImageView glass;
        private ItemClickListener mItemClickListener;

        public ViewHolder(View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.mItemClickListener = itemClickListener;
            ButterKnife.inject(this, itemView);
        }

        /**
         * Set Shot instance related to this viewHolder to be used in ItemClickListener.
         */
        private Shot mShot;
        public void setShot(Shot shot) {
            this.mShot = shot;
        }

        /**
         * Root view click listener;
         */
        @OnClick(R.id.shot_root) void clickListener() {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClicked(this.mShot);
            }
        }
    }

    @Override
    public ShotsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(this.mInflater.inflate(R.layout.shot_row, viewGroup, false),
            this.mItemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Shot shot = this.mShots.get(i);
        viewHolder.setShot(shot);
        Picasso.with(this.mContext).load(shot.getImageUrl()).into(viewHolder.image);
        /* */ Picasso.with(this.mContext).load(shot.getImageUrl())
            .transform(new FastBlurredBitmapTransformation())
            .into(viewHolder.glass); /* */
        viewHolder.titleTxt.setText(shot.getTitle());
        viewHolder.viewsCountTxt.setText(shot.getViewsCount()+"");
        viewHolder.commentsCountTxt.setText(shot.getCommentsCount()+"");
        viewHolder.likesCountTxt.setText(shot.getLikesCount()+"");

        if (i >= (mItemsPerPage*(mCurrentPage-1))-1 && mCurrentPage < mPages && !isLoading) {
            loadShots();
        }
    }


    @Override
    public int getItemCount() {
        return mShots.size();
    }

    /**
     * Load Shots from Dribble API.
     */
    private void loadShots() {
        isLoading = true;
        String url = String.format(API.POPULAR_SHOTS, mCurrentPage);
        JsonObjectRequest request = new JsonObjectRequest(url, null,
            mResponseListener, mErrorListener);
        App.getInstance().addToRequestQueue(request);
        ((ShotsActivity) mContext).startLoading();
    }

    /**
     * Volley Response Listener.
     */
    private Response.Listener<JSONObject> mResponseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {
                JSONArray shots = response.getJSONArray("shots");
                int i = 0; int count = shots.length();
                for (; i < count; i++) {
                    mShots.add(mGson.fromJson(shots.getJSONObject(i).toString(), Shot.class));
                }
                mCurrentPage++;
                mItemsPerPage = response.getInt("per_page");
                mPages = response.getInt("pages");
                isLoading = false;
                ((ShotsActivity) mContext).stopLoading();
                notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Volley Error Listener.
     */
    private Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(App.TAG, error.toString());
            ((ShotsActivity) mContext).stopLoading();
        }
    };

    /**
     * Item click listener.
     */
    public interface ItemClickListener {
        public void onItemClicked(Shot shot);
    }
}
