package com.sambarboza.br.desafioconcretesolutions.model;

import com.google.gson.annotations.Expose;

public class Player {

    @Expose String name;
    @Expose String avatar_url;

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatar_url;
    }
}
