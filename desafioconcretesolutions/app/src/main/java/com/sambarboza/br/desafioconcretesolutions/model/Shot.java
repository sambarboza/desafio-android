package com.sambarboza.br.desafioconcretesolutions.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class Shot implements Serializable {

    @Expose int id;
    @Expose String title;
    @Expose String description;
    @Expose int likes_count;
    @Expose int comments_count;
    @Expose int views_count;
    @Expose String image_url;
    @Expose Player player;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getLikesCount() {
        return likes_count;
    }

    public int getCommentsCount() {
        return comments_count;
    }

    public String getImageUrl() {
        return image_url;
    }

    public int getViewsCount() {
        return views_count;
    }

    public Player getPlayer() { return player; }

}
