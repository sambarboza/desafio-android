package com.sambarboza.br.desafioconcretesolutions;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.sambarboza.br.desafioconcretesolutions.adapter.ShotsAdapter;
import com.sambarboza.br.desafioconcretesolutions.fragment.ShotDetailFragment;
import com.sambarboza.br.desafioconcretesolutions.model.Shot;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class ShotsActivity extends ActionBarActivity {

    @InjectView(R.id.shots_recycler_view) RecyclerView mShotsRecyclerView;
    @InjectView(R.id.progress_bar) SmoothProgressBar mProgressBar;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shots);
        ButterKnife.inject(this);
        getSupportActionBar().setTitle(R.string.popular);
        ShotsAdapter shotsAdapter = new ShotsAdapter(this, this.mShotClickListener);
        mShotsRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mShotsRecyclerView.setLayoutManager(mLayoutManager);
        mShotsRecyclerView.setAdapter(shotsAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Start progressbar animation.
     */
    public void startLoading() {
        this.mProgressBar.progressiveStart();
    }

    /**
     * Stop progressbar animation.
     */
    public void stopLoading() {
        this.mProgressBar.progressiveStop();
    }

    /**
     * ShotsAdapter Click listener.
     */
    ShotsAdapter.ItemClickListener mShotClickListener = new ShotsAdapter.ItemClickListener() {
        @Override
        public void onItemClicked(Shot shot) {
            ShotDetailFragment shotDetailFragment = new ShotDetailFragment();
            Bundle args = new Bundle();
            args.putSerializable("shot", shot);
            shotDetailFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fragment_in, R.anim.fragment_out,
                    R.anim.fragment_in, R.anim.fragment_out)
                .add(R.id.fragments_container, shotDetailFragment)
                .addToBackStack("shot")
                .commit();
        }
    };
}
