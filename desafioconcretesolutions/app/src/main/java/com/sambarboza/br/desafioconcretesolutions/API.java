package com.sambarboza.br.desafioconcretesolutions;

public class API {
    public final static String HOST = "http://api.dribbble.com";
    public final static String POPULAR_SHOTS = HOST + "/shots/popular?page=%d";
    public final static String SHOT = "/shots/%d";
}
