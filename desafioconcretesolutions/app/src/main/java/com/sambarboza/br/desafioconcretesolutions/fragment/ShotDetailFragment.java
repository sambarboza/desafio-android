package com.sambarboza.br.desafioconcretesolutions.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.felipecsl.gifimageview.library.GifImageView;
import com.sambarboza.br.desafioconcretesolutions.R;
import com.sambarboza.br.desafioconcretesolutions.ShotsActivity;
import com.sambarboza.br.desafioconcretesolutions.model.Player;
import com.sambarboza.br.desafioconcretesolutions.model.Shot;
import com.sambarboza.br.desafioconcretesolutions.ui.FastBlurredBitmapTransformation;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShotDetailFragment extends Fragment {

    private Context mContext;
    Shot mShot;
    @InjectView(R.id.shot_detail_image) GifImageView mImage;
    @InjectView(R.id.shot_detail_blur_bg) ImageView mBlurBg;
    @InjectView(R.id.shot_detail_player_avatar) ImageView mPlayerAvatar;
    @InjectView(R.id.shot_detail_title) TextView mTitleTxt;
    @InjectView(R.id.shot_detail_player) TextView mPlayerTxt;
    @InjectView(R.id.shot_detail_description) TextView mDescriptionTxt;
    @InjectView(R.id.shot_details_views) TextView mViewsTxt;
    @InjectView(R.id.shot_details_likes) TextView mLikesTxt;
    @InjectView(R.id.shot_details_comments) TextView mCommentsTxt;


    @Override
    public void setArguments(Bundle args) {
        this.mShot = (Shot) args.getSerializable("shot");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.shot_detail, container, false);
        ButterKnife.inject(this, v);
        this.mContext = getActivity();
        Player player = mShot.getPlayer();

        Picasso.with(mContext).load(mShot.getImageUrl())
            .into(mImage);
        Picasso.with(mContext).load(player.getAvatarUrl())
            .transform(new FastBlurredBitmapTransformation()).into(mBlurBg);
        Picasso.with(mContext).load(player.getAvatarUrl()).into(mPlayerAvatar);

        mTitleTxt.setText(mShot.getTitle());
        mPlayerTxt.setText(player.getName());
        if (mShot.getDescription() != null) {
            mDescriptionTxt.setText(Html.fromHtml(mShot.getDescription()));
        }
        mViewsTxt.setText(mShot.getViewsCount()+"");
        mLikesTxt.setText(mShot.getLikesCount()+"");
        mCommentsTxt.setText(mShot.getCommentsCount()+"");
        this.loadGif(mShot.getImageUrl());
        return v;
    }

    /**
     * Load GIF if available.
     * @param urlStr
     */
    void loadGif(final String urlStr) {
        ((ShotsActivity) mContext).startLoading();
        Task.callInBackground(new Callable<InputStream>() {
            @Override
            public InputStream call() throws Exception {
                URL url = new URL(urlStr);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                return is;
            }
        }).continueWith(new Continuation<InputStream, Void>() {
            public Void then(Task<InputStream> object) throws Exception {
                ((ShotsActivity) mContext).stopLoading();
                byte[] bytes = IOUtils.toByteArray(object.getResult());
                mImage.setBytes(bytes);
                mImage.startAnimation();
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }


    ActionBar mActionBar;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Change Activity ActionBar title after Attach.
        this.mActionBar = ((ActionBarActivity) activity).getSupportActionBar();
        if (this.mActionBar != null) {
            this.mActionBar.setDisplayHomeAsUpEnabled(true);
            this.mActionBar.setTitle(mShot.getTitle());
        }
    }

    @Override public void onDetach() {
        super.onDetach();
        //Change Activity ActionBar title on detach.
        if (this.mActionBar != null) {
            this.mActionBar.setDisplayHomeAsUpEnabled(false);
            this.mActionBar.setTitle(R.string.popular);
        }

        //Stop gif is image is gif.
        this.mImage.stopAnimation();
    }


}
