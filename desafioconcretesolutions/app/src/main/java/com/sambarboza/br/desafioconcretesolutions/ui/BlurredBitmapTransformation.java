package com.sambarboza.br.desafioconcretesolutions.ui;

import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

import com.sambarboza.br.desafioconcretesolutions.App;
import com.squareup.picasso.Transformation;

public class BlurredBitmapTransformation implements Transformation{

    @Override
    public Bitmap transform(Bitmap bitmapOriginal) {
        try {
            RenderScript rs = RenderScript.create(App.getInstance().getApplicationContext());
            final Allocation input = Allocation.createFromBitmap(rs, bitmapOriginal); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
            final Allocation output = Allocation.createTyped(rs, input.getType());
            final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setRadius(25f);
            script.setInput(input);
            script.forEach(output);
            output.copyTo(bitmapOriginal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmapOriginal;
    }

    @Override
    public String key() {
        return "blurredBg()";
    }
}
